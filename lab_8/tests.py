from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

# Create your tests here.
class Lab8UnitTest(TestCase):

	def test_index(self):
		resp = Client().post('/lab-8/')
		self.assertEqual(resp.status_code, 200);