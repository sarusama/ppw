var erase;
var print;

$(document).ready(function(){
  erase = false;
	print = document.getElementById('print');

	var init = [
		{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
		{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"},
	];

	window.localStorage.setItem('list', JSON.stringify(init));
	window.localStorage.setItem('selectedTheme', '{"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}}')

	$('.my-select').select2({
    'data': JSON.parse(window.localStorage.getItem('list'))
	});
	$('.apply-button').on('click', function(){
    // [TODO] ambil value dari elemen select .my-select
    var id = $('.my-select').val();
    // [TODO] ambil object theme yang dipilih
    var theme = JSON.parse(window.localStorage.getItem('list'))[id];
    console.log(id);
    console.log(theme);
    window.localStorage.setItem('selectedTheme', JSON.stringify(theme));
    applyTheme();
	})
  $(".chat-head").click(function(){
      $(".chat-body").toggle();
  });
});

var go = function(x) {
  if (erase === true) {
  	print.value = ''
  }

  if (x === 'ac') {
    erase = true;
  } else if (x === 'eval') {
  	print.value = eval(print.value);
    erase = true;
  } else {
    print.value += x;
    erase = false;
  }
};

function eval(fn) {
  return new Function('return ' + fn)();
}

function applyTheme() {
	var theme = JSON.parse(window.localStorage.getItem('selectedTheme'));
	$('body').css('background-color', theme.bcgColor);
}
