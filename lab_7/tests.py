# from django.test import TestCase
# from django.test import Client
# from django.urls import resolve
# from .views import index
# from .models import Friend
# from .api_csui_helper.csui_helper import CSUIhelper

# # Create your tests here.
# class Lab7UnitTest(TestCase):

#     def test_get_client_id(self):
#         self.assertEqual(CSUIhelper().instance.get_client_id(),'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG')

#     def test_get_auth_param_dict(self):
#         CSUIhelper.instance.get_auth_param_dict()

#     def test_sso_wrong_password(self):
#         CSUIhelper(username='kappa', password='123')

#     def test_add_friend(self):
#         Client().post('/lab-7/add-friend/', {'name':'kappa', 'npm':'123'})
#         self.assertEqual(Friend.objects.all().count(), 1)

#     def test_add_friend_duplicate(self):
#         Client().post('/lab-7/add-friend/', {'name':'kappa', 'npm':'123'})
#         Client().post('/lab-7/add-friend/', {'name':'kappa', 'npm':'123'})
#         self.assertEqual(Friend.objects.all().count(), 1)

#     def test_index(self):
#         resp = Client().get('/lab-7/')
#         self.assertEqual(resp.status_code, 200)

#     def test_delete_friend(self):
#         Client().post('/lab-7/add-friend/', {'name':'kappa', 'npm':'123'})
#         Client().post('/lab-7/delete-friend/1/')
#         self.assertEqual(Friend.objects.all().count(), 0)

#     def test_validate_npm(self):
#         resp = Client().post('/lab-7/add-friend/', {'name':'kappa', 'npm':'123'})
#         value = Client().post('/lab-7/validate-npm/', {'npm':'123'})
#         self.assertEqual(resp.status_code, 200)

#     def test_friend_list(self):
#         resp = Client().post('/lab-7/friend-list/')
#         self.assertEqual(resp.status_code, 200)

#     def test_friend_list_json(self):
#         resp = Client().post('/lab-7/get-friend-list/')
#         self.assertEqual(resp.status_code, 200)